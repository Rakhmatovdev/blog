import Image from "next/image";
import PostItem from "./components/Post";

interface CardObj {
  id: string;
  title: string;
  username: string;
  description: string;
  image: string;
  user: string;
}

export default function Home() {
  const CardData: CardObj[] = [
    {
      id: "1",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project1.png",
      user: "/card/user1.png",
    },
    {
      id: "2",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project2.png",
      user: "/card/user2.png",
    },
    {
      id: "3",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project3.png",
      user: "/card/user3.png",
    },
    {
      id: "4",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project4.png",
      user: "/card/user4.png",
    },
    {
      id: "5",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project5.png",
      user: "/card/user5.png",
    },
    {
      id: "6",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project6.png",
      user: "/card/user1.png",
    },
    {
      id: "7",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project7.png",
      user: "/card/user2.png",
    },
    {
      id: "8",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project8.png",
      user: "/card/user3.png",
    },
    {
      id: "9",
      title: "Technologiy",
      username: "JR",
      description:
        "The Impact of Technology on the Workplace: How Technology is Changing",
      image: "/card/project9.png",
      user: "/card/user4.png",
    },
  ];

  return (
    <section>
      <div className="">
        <div className="hero mt-2 rounded-xl h-[600px] p-36 bg-[url('/hero/hero.png')] bg-cover bg-no-repeat bg-center relative mb-36">
          <div className="max-w-[600px]  bg-white p-11 rounded-xl absolute -bottom-16 shadow-lg">
            <button className="bg-indigo-500 text-white  font-bold rounded-md px-3 py-1 mb-4">
              Technology
            </button>
            <div className="title mb-5 text-[34px] tracking-wide font-bold leading-10 ">
              The Impact of Technology on the Workplace: How Technology is
              Changing
            </div>
            <div className="hero-user flex items-center gap-3">
              <Image
                src={"/card/user1.png"}
                alt={"User image"}
                width={36}
                height={36}
              />
              <p className="text-base text-stone-500">Jason Francisco</p>
              <p className="text-base text-stone-500">August 20, 2022</p>
            </div>
          </div>
        </div>

        <div className="">
          <h2 className="  font-black text-lg tracking-wider mb-8">Latest Post</h2>
          <div className="grid grid-cols-3 gap-5">
            {CardData &&
              CardData.map((post) => {
                return (
                  <PostItem
                    key={post.id}
                    id={post.id}
                    image={post.image}
                    title={post.description}
                    category={post.title}
                  />
                );
              })}
          </div>

          {/* <div className="flex justify-between flex-wrap gap-4">
            {CardData?.map((post)=>{
            return (<div key={post.id}>
            <div className="max-w-sm bg-white border w-[300px] border-gray-200 rounded-lg shadow dark:border-gray-200">
            <a href="/">
              <Image
                className="rounded-t-lg w-full"
                src={post.image}
                alt={post.title}
                width={300}
                height={200}
              />
            </a>
            <div className="p-5">
              <a href="#">
                <h5 className="mb-2 text-2xl font-bold tracking-tight ">
                 {post.title}
                </h5>
              </a>
              <p className="mb-3 font-normal text-gray-400">
               {post.description}
              </p>
              <div className="flex justify-between items-center">
                <p className="flex items-center gap-2">
               <Image src={post.user} alt="posted user" width={40} height={40}/>
               <p className="text-stone-700">{post.username}</p>
               </p>
               <div className="text-stone-200">26.02.2026</div>
              </div>
            </div>
          </div></div>)
          })}
          </div> */}
          {/* <div className="text-center mt-8">
            <button className="px-4 py-2 border border-stone-200   ">
              View All
            </button>
          </div> */}
        </div>
      </div>
    </section>
  );
}
