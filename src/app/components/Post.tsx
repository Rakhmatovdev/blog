import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

interface PostItemProp{
    id:string,
    image:string,
    category:string,
    title:string,
    // creator:{
    //     image:string,
    //     email:string,
    //     username:string,
    // },
    // date:string

}

const PostItem = ({image,category,title,id}:PostItemProp) => {
  return (
    <div className='rounded-lg p-4 border'>
        <Image src={image} width={360} height={240} alt={"Post Image"} className='rounded-md object-cover w-full mb-4'/>
    <button className='px-3 py-1 rounded-md bg-indigo-50 text-indigo-600 text-sm mb-4 font-bold'>Technology</button>
    <Link href={id} className='hover:underline line-clamp-3 font-bold text-2xl mb-5'>The Impact of Technology on the Workplace: How Technology is Changing</Link>
    <div className="hero-user flex items-center gap-3">
            <Image
              src={"/card/user1.png"}
              alt={"User image"}
              width={36}
              height={36}
            />
            <p className="text-base text-stone-500">Jason Francisco</p>
            <p className="text-base text-stone-500">August 20, 2022</p>
          </div>
    
    </div>
  )
}

export default PostItem