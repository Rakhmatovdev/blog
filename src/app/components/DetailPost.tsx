import Image from "next/image";

interface PostDetailProp {
  id: string;
  image: string;
  category: string;
  title: string;
  text: string[];
  // creator:{
  //     image:string,
  //     email:string,
  //     username:string,
  // },
  // date:string
}

const DetailPost = ({ image, category, title, text, id }: PostDetailProp) => {
  return (
    <section className="detail">
      <div className="container mx-auto max-w-[800px]">
        <div className="detail-con pt-8">
          <button className="bg-indigo-500 text-white  rounded-md px-3 py-1 mb-6 ">
            Technology
          </button>
          <h1 className="text-4xl tracking-wide font-bold">
            The Impact of Technology on the Workplace: How Technology is
            Changing
          </h1>
          <div className="hero-user flex items-center gap-3 mt-5">
            <Image
              src={"/card/user1.png"}
              alt={"User image"}
              width={36}
              height={36}
            />
            <p className="text-base text-stone-500">Jason Francisco</p>
            <p className="text-base text-stone-500">August 20, 2022</p>
          </div>
        </div>
        <div className="">
          <Image
            src={image}
            width={800}
            height={462}
            alt={"blog post image"}
            className="my-8 object-cover"
          />
          {text.map((item,index)=>{
            return (<p key={index} className="mb-5 indent-4 break-all">{item}</p>)
          })}
        </div>
      </div>
    </section>
  );
};

export default DetailPost;
