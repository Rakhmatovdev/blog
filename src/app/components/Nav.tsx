"use client";
import { signIn, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
const NavbarAuth = () => {
  const { status } = useSession();

  return (
    <nav className=" w-full  shadow-md  py-4 px-0 text-stone-950 ">
      <div className="max-w-6xl mx-auto flex justify-between items-center ">
        <div className="text-2xl">
          <Link href={"/"}>
            <Image src="/Logo.png" alt={"logo"} width={120} height={120} />
          </Link>
        </div>
        <ul className="flex justify-between items-center gap-10">
          <Link href={"/"} className="font-bold text-base ">
            Home
          </Link>
          <Link href={"/create-post"} className="font-bold text-base ">
            Create post
          </Link>
          <Link href={"/update-post"} className="font-bold text-base ">
            Update post
          </Link>
          {status === "authenticated" ? (
            <Link href={"/dashboard"} className="font-bold text-xl ">
              Dashboard
            </Link>
          ) : null}
        </ul>
        <div className="flex gap-2">
          <label className="inline-flex items-center cursor-pointer">
            <input type="checkbox" value="" className="sr-only peer" />
            <div className="relative w-11 h-6 after:flex after:justify-center bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white afler after:content-[url('/sunny.svg')] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600">
             
            </div>
            <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300"></span>
          </label>

          {
        status==='authenticated'?(<button className='border bg-sky-600 px-6 py-3 rounded-md '  onClick={() => signOut()}>
        Sign Out
    </button>):<><button className='border flex bg-stone-50 px-2 py-1 rounded-md ' onClick={()=>signIn('google')}>
        <p> Google</p> <img src="/google.svg" alt="google" />
    </button>
    <button className='border bg-stone-50 px-2 py-1 rounded-md flex' onClick={()=>signIn('github')}>
       <p> Github</p> <img src="/github.svg" alt="github icon" />
    </button>
    </> 
    }

          {/* <button
            className="border bg-sky-600 px-2 py-1 rounded-md"
            onClick={() => signIn()}
          >
            Sign In with Other
          </button> */}
        </div>
      </div>
    </nav>
  );
};

export default NavbarAuth;
