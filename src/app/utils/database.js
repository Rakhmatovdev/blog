import { log } from 'console'
import mongoose from 'mongoose'

async function connectToDB(){
try {
    await mongoose.connect(process.env.MONGODB_URL,{dbName:'blog'})
    log("MongoDB connected ...")
} catch (error) {
    log(error)
}
}

export default connectToDB