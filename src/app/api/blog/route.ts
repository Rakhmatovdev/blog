import { NextRequest } from 'next/server';
import connectToDB from '../../utils/database';
import { Blog } from '../../models/blog';
import { log } from 'console';
export async function POST(req:NextRequest) {
   const {title,postImage,postText,authorId,catName}= await req.json()
   try {
    await connectToDB()
    const blog=new Blog({
      title,postImage,postText,authorId,catName
    })
    blog.save()
     return Response.json({status:200,massage:"New user created"})
   } catch (error) {
    log(error)
    return Response.json({status:500,massage:"Something weng wron !"})
   }
}

export async function GET() {
  try {
  
    await connectToDB()
    const res= await Blog.find()
    return Response.json({status:200,massage:res})
  } catch (error) {
    return Response.json({status:500,massage:"Something weng wron !"})
    
  }
}