import mongoose, { models } from "mongoose";

const blogSchema = new mongoose.Schema(
  {
    title: String,
    catName: String,
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Users",
    },
    postImage: String,
    postText: String,
  },
  { timestamps: true }
);

export const Blog = models.BlogItem || mongoose.model("BlogItem", blogSchema);
