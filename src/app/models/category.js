import mongoose, { models } from "mongoose";

const categorySchema=new mongoose.Schema({
name:String,
catPosts:[String],
email:String,
img:String,
},
{timestamps:true})

export const Category=models.Category || mongoose.model("Category",categorySchema)