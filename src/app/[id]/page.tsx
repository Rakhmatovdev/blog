import DetailPost from "../components/DetailPost";

interface PostDetailProp {
  id: string;
  image: string;
  username: string;
  description: string;
  title: string;
  user: string;
  text: string[];
}
const DetailBlog = ({ params }: { params: { id: string } }) => {
  const data: PostDetailProp = {
    id: "1",
    title: "Technologiy",
    username: "JR",
    description:
      "The Impact of Technology on the Workplace: How Technology is Changing",
    image: "/card/project1.png",
    user: "/card/user1.png",
    text: [
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem unde neque quod possimus voluptatibus praesentium tempore! Alias ipsum nostrum, repudiandae voluptas voluptatibus officiis iusto illum perferendis.",
"Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora ducimus natus repellendus deserunt sint pariatur nihil laudantium. Ipsum, impedit? Culpa asperiores in voluptatum aperiam! Omnis adipisci perspiciatis harum, corporis soluta earum pariatur. Praesentium.",
"Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae accusamus quis minus nisi labore, quia non! Laborum unde ut accusamus!",
"Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ab nam quidem nulla sunt explicabo itaque.",
" Lorem ipsum dolor sit amet consectetur adipisicing elit. Est sapiente totam obcaecati rerum possimus. Nihil obcaecati libero impedit commodi amet excepturi, cum, aperiam eveniet quasi voluptates itaque modi error ad molestias repudiandae officia. Aspernatur dolor sapiente suscipit quasi iusto illo, consectetur perspiciatis, nulla quas eaque repudiandae tempore ipsa! Possimus, consequuntur vero similique fugiat voluptas assumenda."   
],
  };
 

  return (
    <div>
      <DetailPost
        id={params.id}
        image={data.image}
        category={data.title}
        title={data.description}
        text={data.text}
      />
    </div>
  );
};

export default DetailBlog;
